"use strict";

// 1.Описать своими словами для чего вообще нужны функции в программировании.
// Функции в програмировании используются для разделения кода на части, для удобства чтения и восприятия.
// Также, одну и ту же опперацию мы часто повторяем, вместо того чтоб заново записывать ее, ее проще просто вызывать.

// 2. Описать своими словами, зачем в функцию передавать аргумент.
// Аргумент можно и не передавать, зависит от функции. Передаем его, для выполнения с ним неких оппераций и возврата нам результата.

function calcNums(num1, num2, sign) {
  let res = 0;
  if (sign === "+") {
    res = num1 + num2;
  } else if (sign === "-") {
    res = num1 - num2;
  } else if (sign === "*") {
    res = num1 * num2;
  } else {
    res = num1 / num2;
  }
  return res;
}

function checkSign(sign) {
  if (sign === "+") {
    return true;
  } else if (sign === "-") {
    return true;
  } else if (sign === "*") {
    return true;
  } else if (sign === "/") {
    return true;
  } else {
    return false;
  }
}

let userFirstNum = prompt("First number:");
let userSecondNum = prompt("Second number:");
let userSign = prompt("Enter sign:");

while (
  userFirstNum === "" ||
  userSecondNum === "" ||
  userFirstNum === null ||
  userSecondNum === null ||
  isNaN(+userFirstNum) ||
  isNaN(+userSecondNum) ||
  !checkSign(userSign)
) {
  if (userFirstNum === null) {
    userFirstNum = "";
  }
  if (userSecondNum === null) {
    userSecondNum = "";
  }
  if (userSign === null) {
    userSign = "";
  }
  userFirstNum = prompt("First number:", userFirstNum);
  userSecondNum = prompt("Second number:", userSecondNum);
  userSign = prompt("Enter sign:", userSign);
}

console.log(calcNums(+userFirstNum, +userSecondNum, userSign));
